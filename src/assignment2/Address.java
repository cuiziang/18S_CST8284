package assignment2;

class Address {

    private String streetNumber;
    private String streetName;
    private String streetType;
    private String streetOrientation;
    private String cityName;
    private String province;

    Address() {
        this.streetNumber = "";
        this.streetName = "";
        this.streetType = "";
        this.streetOrientation = "";
        this.cityName = "";
        this.province = "";
    }

    String getStreetNumber() {
        return streetNumber;
    }

    void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    String getStreetName() {
        return streetName;
    }

    void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    String getStreetType() {
        return streetType;
    }

    void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    String getStreetOrientation() {
        return streetOrientation;
    }

    void setStreetOrientation(String streetOrientation) {
        this.streetOrientation = streetOrientation;
    }

    String getCityName() {
        return cityName;
    }

    void setCityName(String cityName) {
        this.cityName = cityName;
    }

    String getProvince() {
        return province;
    }

    void setProvince(String province) {
        this.province = province;
    }
}
