package assignment2;

class Person {

    private String firstName;
    private String lastName;
    private String spouseFirstName;
    private String spouseLastName;

    Person() {
        this.firstName = "";
        this.lastName = "";
        this.spouseFirstName = "";
        this.spouseLastName = "";
    }

    String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    String getSpouseFirstName() {
        return spouseFirstName;
    }

    void setSpouseFirstName(String spouseFirstName) {
        this.spouseFirstName = spouseFirstName;
    }

    String getSpouseLastName() {
        return spouseLastName;
    }

    void setSpouseLastName(String spouseLastName) {
        this.spouseLastName = spouseLastName;
    }
}
