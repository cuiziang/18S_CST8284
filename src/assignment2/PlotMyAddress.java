package assignment2;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlotMyAddress {

    private static final String BASED_API = "https://maps.googleapis.com/maps/api/geocode/json?";
    private static final String KEY = "AIzaSyCU75pdEliXfKVj3PIPk2RRUwSk2KX1K2c";

//    Uncomment it when use Windows
//    private static final String RAW_ADDRESS_FILE = "C:\\\\CST8284\\input\\InputAddresses.txt";
//    private static final String CVS_ADDRESS_FILE = "C:\\\\CST8284\\output\\OutputAddresses.csv";
//    private static final String CVS_LOCATION_FILE = "C:\\\\CST8284\\output\\LatLong.csv";

//    Uncomment it when use MacOS
    private static final String RAW_ADDRESS_FILE = "resources/InputAddresses.txt";
    private static final String CVS_ADDRESS_FILE = "resources/OutputAddresses.csv";
    private static final String CVS_LOCATION_FILE = "resources/LatLong.csv";

    private static Map<Person, Address> addressMap;
    private static Map<Person, String> latitudeMap;
    private static Map<Person, String> longitudeMap;

    private static void readRawAddressFile() {

        try {
            addressMap = new LinkedHashMap<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(RAW_ADDRESS_FILE));

            String inputLine;
            Matcher matcher;

            Person person = new Person();
            Address address = new Address();

            int lineNumber = 0;

            while (null != (inputLine = bufferedReader.readLine())) {

                if (inputLine.equals("")) {
                    addressMap.put(person, address);
                    person = new Person();
                    address = new Address();
                    lineNumber = 0;
                    continue;
                }

                if (lineNumber == 0) {
                    if (inputLine.contains("and")) {
                        matcher = Pattern.compile("(\\w+)\\sand\\s(\\w+)\\s(\\w+)").matcher(inputLine);
                        while (matcher.find()) {
                            person.setFirstName(matcher.group(1));
                            person.setLastName(matcher.group(3));
                            person.setSpouseFirstName(matcher.group(2));
                            person.setSpouseLastName(matcher.group(3));
                        }
                    } else if (inputLine.contains(",")) {
                        matcher = Pattern.compile("(\\w+)\\s(\\w+),\\s(\\w+)\\s(\\w+)").matcher(inputLine);
                        while (matcher.find()) {
                            person.setFirstName(matcher.group(1));
                            person.setLastName(matcher.group(2));
                            person.setSpouseFirstName(matcher.group(3));
                            person.setSpouseLastName(matcher.group(4));
                        }
                    } else {
                        matcher = Pattern.compile("(\\w+)\\s(\\w+)").matcher(inputLine);
                        while (matcher.find()) {
                            person.setFirstName(matcher.group(1));
                            person.setLastName(matcher.group(2));
                        }
                    }
                } else if (lineNumber == 1) {
                    matcher = Pattern.compile("(?:\\d+-)?(\\d+)\\s(\\w+)\\s(\\w+)\\s?(\\w*)").matcher(inputLine);
                    while (matcher.find()) {
                        address.setStreetNumber(matcher.group(1));
                        address.setStreetName(matcher.group(2));
                        address.setStreetType(matcher.group(3));
                        address.setStreetOrientation(matcher.group(4));
                    }
                } else if (lineNumber == 2) {
                    matcher = Pattern.compile("(\\w+)(?:,\\s)(\\w+)").matcher(inputLine);
                    while (matcher.find()) {
                        address.setCityName(matcher.group(1));
                        address.setProvince(matcher.group(2));
                    }
                }
                lineNumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeCVSAddressFile() {
        try {
            File file = new File(CVS_ADDRESS_FILE);
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (Person person : addressMap.keySet()) {
                String line = String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", person.getFirstName(), person.getLastName(), person.getSpouseFirstName(), person.getSpouseLastName(), addressMap.get(person).getStreetNumber(), addressMap.get(person).getStreetName(), addressMap.get(person).getStreetType(), addressMap.get(person).getStreetOrientation(), addressMap.get(person).getCityName(), addressMap.get(person).getProvince());
                bufferedWriter.write(line);
            }
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readCVSAddressFile() {

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(CVS_ADDRESS_FILE));

            bufferedReader.lines().forEach(line -> {
                String[] addressArray;

                addressArray = line.split(",");

                Person person = new Person();
                Address address = new Address();

                person.setFirstName(addressArray[0]);
                person.setLastName(addressArray[1]);
                person.setSpouseFirstName(addressArray[2]);
                person.setSpouseLastName(addressArray[3]);

                address.setStreetNumber(addressArray[4]);
                address.setStreetName(addressArray[5]);
                address.setStreetType(addressArray[6]);
                address.setStreetOrientation(addressArray[7]);
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void fetchLocation() {
        latitudeMap = new HashMap<>();
        longitudeMap = new HashMap<>();

        addressMap.keySet().forEach(person -> {
            try {
                StringBuilder jSONstring = new StringBuilder();
                InputStream inputStream = new URL(BASED_API
                        + "address="
                        + addressMap.get(person).getStreetNumber() + "+"
                        + addressMap.get(person).getStreetName() + "+"
                        + addressMap.get(person).getStreetType() + "+"
                        + addressMap.get(person).getStreetOrientation() + "+"
                        +"Ottawa,"+"+"
                        +"On,"+"+"
                        +"Canada"
                        + "&key=" + KEY).
                        openConnection().getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                bufferedReader.lines().forEach(jSONstring::append);

                JsonParser parser = new JsonParser();
                JsonObject jsonObject = parser.parse(jSONstring.toString()).getAsJsonObject();

                latitudeMap.put(person, jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lat").getAsString());
                longitudeMap.put(person, jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("geometry").getAsJsonObject().get("location").getAsJsonObject().get("lng").getAsString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    private static void writeCVSLocationFile() {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(CVS_LOCATION_FILE)));

            bufferedWriter.write("Latitude,Longitude,Name,Icon,IconScale,IconAltitude\n");

            addressMap.keySet().forEach(person -> {
                String name = person.getFirstName() + " " + person.getLastName();
                if (!person.getSpouseFirstName().equals("") || !person.getSpouseLastName().equals("")) {
                    name += " and " + person.getFirstName() + " " + person.getLastName();
                }
                String line = String.format("%s,%s,%s,111,1,1\n", latitudeMap.get(person), longitudeMap.get(person), name);
                try {
                    bufferedWriter.write(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        readRawAddressFile();
        writeCVSAddressFile();
        readCVSAddressFile();
        fetchLocation();
        writeCVSLocationFile();
    }
}
