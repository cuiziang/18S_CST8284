package assignment3;

import java.io.*;
import java.util.*;

public class WordOccurences {
    public static void main(String[] args) {

        Map<String, Integer> wordOccurencesMap = new LinkedHashMap<>();

        Scanner input = new Scanner(System.in);
        System.out.println("Enter text file include path...");

        String inputFile = input.nextLine();
        String[] inputFileArray = inputFile.split(".txt");
        StringBuilder fileAsString = new StringBuilder();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));

            bufferedReader.lines().forEach(fileAsString::append);

            List<String> wordArray = Arrays.asList(fileAsString.toString().replaceAll("\\pP", "").toLowerCase().split("\\s+"));

            final int[] longest = {0};

            wordArray.forEach(word -> {
                if (word.length()> longest[0]){
                    longest[0] =word.length();
                }
                if (wordOccurencesMap.containsKey(word)) {
                    wordOccurencesMap.put(word, wordOccurencesMap.get(word) + 1);
                } else {
                    wordOccurencesMap.put(word, 1);
                }
            });


            File outputFile = new File(inputFileArray[0] + "_Out.txt");
            FileWriter fileWriter = new FileWriter(outputFile);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            wordOccurencesMap.forEach((word, occurences) -> {
                try {
                    bufferedWriter.write(String.format("%-"+(longest[0])+"s\t%d\n", word, occurences));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
