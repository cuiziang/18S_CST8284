package assignment4;

public class MyLoggerTest {
    public static void main(String[] args) {
        // Initialize MyLogger class appropriately (one line of code)
        MyLogger myLogger = new MyLogger();
        // Use System.setOut method to set your output stream appropriately (one line of code)
        System.setOut(myLogger);
        boolean b = true;
        char c = 'c';
        char[] s1 = {'a', 'b', 'c','d','e'};
        double d = 34.5;
        float f=45.4f;
        int i=15;
        long l=12345678;
        String s2 = "My name is Michel";

        System.out.println(i);
        System.out.println(b);
        System.out.println(c);
        System.out.println(s1);
        System.out.println(d);
        System.out.println(f);
        System.out.println(l);
        System.out.println(myLogger);
        System.out.println(s2);

        System.out.print(i);
        System.out.print(b);
        System.out.print(c);
        System.out.print(s1);
        System.out.print(d);
        System.out.print(f);
        System.out.print(l);
        System.out.print(myLogger);
        System.out.print(s2);
    }
}
