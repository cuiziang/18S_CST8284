package assignment4;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

class MyLogger extends PrintStream {

    MyLogger() {
        super(System.out);
    }

    private void printTimestamp(Object object, boolean lineBreak) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, MMMM d, yyyy 'at' HH:mm:ss:SSS z: ");
        super.print(simpleDateFormat.format(new Date()));
        if (object instanceof Boolean) {
            super.print("printing a boolean: " + object);
        } else if (object instanceof Character) {
            super.print("printing a character: ");
            super.print(object);
        } else if (object instanceof Integer) {
            super.print("printing an integer: " + object);
        } else if (object instanceof Long) {
            super.print("printing a long: " + object);
        } else if (object instanceof Float) {
            super.print("printing a long: " + object);
        } else if (object instanceof Double) {
            super.print("printing a double: " + object);
        } else if (object instanceof char[]) {
            super.print("printing an array of characters: ");
            super.print((char[]) object);
        } else if (object instanceof String) {
            super.print("printing a String: " + object);
        } else {
            super.print("printing a object: " + object);
        }
        if (lineBreak) {
            super.print("\n");
        }
    }

    @Override
    public void print(boolean b) {
        printTimestamp(b, false);
    }

    @Override
    public void print(char c) {
        printTimestamp(c, false);
    }

    @Override
    public void print(int i) {
        printTimestamp(i, false);
    }

    @Override
    public void print(long l) {
        printTimestamp(l, false);
    }

    @Override
    public void print(float f) {
        printTimestamp(f, false);
    }

    @Override
    public void print(double d) {
        printTimestamp(d, false);
    }

    @Override
    public void print(char[] s) {
        printTimestamp(s, false);
    }

    @Override
    public void print(String s) {
        printTimestamp(s, false);
    }

    @Override
    public void print(Object obj) {
        printTimestamp(obj, false);
    }

    @Override
    public void println(boolean x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(char x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(int x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(long x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(float x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(double x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(char[] x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(String x) {
        printTimestamp(x, true);
    }

    @Override
    public void println(Object x) {
        printTimestamp(x, true);
    }
}
