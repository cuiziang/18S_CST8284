var mymap = L.map('cst8284').setView([45.4, -75.6], 10);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 15,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var customLayer = L.geoJson(null, {
    onEachFeature: function(feature, layer) {
        layer.bindPopup(feature.properties.Name);
    }
});
var runLayer = omnivore.csv('LatLong.csv', null, customLayer)
    .on('ready', function() {
        // http://leafletjs.com/reference.html#map-fitbounds
        mymap.fitBounds(runLayer.getBounds());
    })
    .addTo(mymap);