package assignment1;

public class Person {

    private String firstName;
    private String lastName;
    private String spouseFirstName;
    private String spouseLastName;

    public Person() {
        this.firstName = "";
        this.lastName = "";
        this.spouseFirstName = "";
        this.spouseLastName = "";
    }

    public void erase() {
        this.firstName = "";
        this.lastName = "";
        this.spouseFirstName = "";
        this.spouseLastName = "";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpouseFirstName() {
        return spouseFirstName;
    }

    public void setSpouseFirstName(String spouseFirstName) {
        this.spouseFirstName = spouseFirstName;
    }

    public String getSpouseLastName() {
        return spouseLastName;
    }

    public void setSpouseLastName(String spouseLastName) {
        this.spouseLastName = spouseLastName;
    }

    @Override
    public String toString() {
        return firstName + ','  + lastName + ',' + spouseFirstName + ','  + spouseLastName;
    }
}
