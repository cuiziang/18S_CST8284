package assignment1;

class Address {

    private String streetNumber;
    private String streetName;
    private String streetType;
    private String streetOrientation;
    private String cityName;
    private String province;

    public Address() {
        this.streetNumber = "";
        this.streetName = "";
        this.streetType = "";
        this.streetOrientation = "";
        this.cityName = "";
        this.province = "";
    }

    public void erase() {
        this.streetNumber = "";
        this.streetName = "";
        this.streetType = "";
        this.streetOrientation = "";
        this.cityName = "";
        this.province = "";
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getStreetOrientation() {
        return streetOrientation;
    }

    public void setStreetOrientation(String streetOrientation) {
        this.streetOrientation = streetOrientation;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return streetNumber + ',' + streetName + ',' + streetType + ',' + streetOrientation + ',' + cityName + ',' + province;
    }
}
