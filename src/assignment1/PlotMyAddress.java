package assignment1;

import java.io.*;

public class PlotMyAddress {

    private static Address a = new Address();
    private static Person p = new Person();


    private static void readFile() {

        String s;
        try {
            FileReader fr = new FileReader("resources/InputAddresses.txt");
            File writename = new File("resources/OutputAddresses.csv");
            BufferedWriter out = new BufferedWriter(new FileWriter(writename));


            BufferedReader br = new BufferedReader(fr);

            int line = 0;

            while (null != (s = br.readLine())) {
                if (s.equals("")) {
                    if (line != 0) {
                        out.write(p.toString()+','+a.toString()+"\n");
                        out.flush();
                        p.erase();
                        a.erase();
                        System.out.println();
                        line = 0;
                    }
                    continue;
                }
                //read name
                if (line == 0) {
                    readNames(s);
                }
                //read street
                if (line == 1) {
                    readStreet(s);
                }
                //read location
                if (line == 2) {
                    readLocation(s);
                }
                line++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void readNames(String names) {
        if (names.contains(" and ") || names.contains(", ")) {
            String[] nameArray;
            nameArray = names.contains(" and ") ? names.split(" and ") : names.split(", ");
            String[] ownerNameArray = nameArray[0].split(" ");
            String[] spouseNameArray = nameArray[1].split(" ");
            p.setFirstName(ownerNameArray[0]);
            if (ownerNameArray.length > 1) {
                p.setLastName(ownerNameArray[1]);
            }
            p.setSpouseFirstName(spouseNameArray[0]);
            if (spouseNameArray.length > 1) {
                p.setSpouseLastName(spouseNameArray[1]);
                p.setLastName(spouseNameArray[1]);
            }
        } else {
            String[] ownerName;
            ownerName = names.split(" ");
            p.setFirstName(ownerName[0]);
            if (ownerName.length > 1) {
                p.setLastName(ownerName[1]);
            }
        }
    }

    private static void readStreet(String street) {
        if (street.contains(" ")) {
            String[] streetArray = street.split(" ");
            a.setStreetNumber(streetArray[0].substring(streetArray[0].indexOf('-') + 1));
            a.setStreetName(streetArray[1]);
            a.setStreetType(streetArray[2]);
            if (streetArray.length > 3) {
                a.setStreetOrientation(streetArray[3]);
            }
        }
    }

    private static void readLocation(String location) {
        if (location.contains(", ")) {
            String[] locationArray;
            locationArray = location.split(", ");
            a.setCityName(locationArray[0]);
            a.setProvince(locationArray[1]);
        }
    }


    public static void main(String[] args) {
        readFile();
    }
}
