package lab2;

public class PayableInterfaceTest {

    public static void main(String args[]) {
        Payable payableObjects[] = new Payable[6];
        payableObjects[0] = new Invoice("01234", "seat", 2, 375.00);
        payableObjects[1] = new Invoice("56789", "tire", 4, 79.95);
        payableObjects[2] =
                new SalariedEmployee("John", "Smith", "111-11-1111", 800.00);
        payableObjects[3] = new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40.00);
        payableObjects[4] = new CommissionEmployee("Sue", "Jones", "333-33-3333", 10000.00, 0.06);
        payableObjects[5] = new BasePlusCommissionEmployee("Bob", "Lewis", "444-44-4444", 5000.00, 0.04, 300);

        System.out.println("Invoices and Employees processed polymorphically:\n");

        for (Payable currentPayable : payableObjects) {
            System.out.printf("%s \n", currentPayable.toString());

            if (currentPayable instanceof BasePlusCommissionEmployee) {
                ((BasePlusCommissionEmployee) currentPayable).setBaseSalary(((BasePlusCommissionEmployee) currentPayable).getBaseSalary()*1.1);
                System.out.printf("%s: $%,.2f\n",
                        "new base salary with 10% increase is: ", ((BasePlusCommissionEmployee) currentPayable).getBaseSalary());
            }
            System.out.printf("%s: $%,.2f\n\n",
                    "payment due", currentPayable.getPaymentAmount());
        }
    }
}
