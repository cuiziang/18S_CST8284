package lab2;

public interface Payable {
    double getPaymentAmount();
}